## Setup

You will be using the .NET Framework or .NET Core

## General requirements and comments

### Time limit

Code challenge should take one hour. Try to do the test within that time-frame, and implement only the most important things to make the functionality work within that time-frame. No need to overdo it, as there is no need for it. Keep the amount of the code produced within that one hour.

The main goal of the code challenge is to see the quality of the code and its correctness.

### Sending the code

After the code challenge is completed create a pull request, add  *rherman84* as a reviewer and submit it.

## Code Challenge

Create a C# Console Application (.NET Framework or .NET Core) which serializes and deserializes a binary tree.

Steps: Define a binary tree structure, implement a *Serialize* method, which serializes the tree into a string, and a *Deserialize* method, which deserializes the string back into the tree. 

*Note: Try to write some test cases to cover the functionality of the written code.*
