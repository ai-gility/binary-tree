﻿using System;
using BinaryTree;
using TreeTest.Helpers;

namespace TreeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create Tree
            var oX3 = TreeNodeFabric.GenerateTreeNode("X3", "L7","R9");
            var oX2 = TreeNodeFabric.GenerateTreeNode("X2", "L6","R8");
            var oX1 = new TreeNode("X1", oX2, oX3);
            var oX4 = new TreeNode("X4", null, oX1);
            var oX5 = TreeNodeFabric.GenerateTreeNode("X5", "L1","R1");
            var oRoot = new TreeNode("root", oX4, oX5);

            //Serialize Tree
            var serialization = TreeNodeFabric.Serialize(oRoot);
            Console.WriteLine("Original Tree");
            Console.Write(XmlHelper.PrettyXml(serialization));
            Console.WriteLine();
            Console.WriteLine();

            //Deserialize Tree
            var oNewRoot = TreeNodeFabric.Deserialize(serialization);
            var serializationNew = TreeNodeFabric.Serialize(oNewRoot);
            Console.WriteLine("Deserialized Tree");
            Console.Write(XmlHelper.PrettyXml(serializationNew));
            Console.WriteLine();

            Console.ReadKey();
        }

       
    }
}