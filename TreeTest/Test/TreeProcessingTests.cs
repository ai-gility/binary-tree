﻿using System;
using BinaryTree;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Test
{
    [TestClass]
    public class TreeProcessingTests
    {
       
        private static void TreeCanBeProcessed(TreeNode tree)
        {
            var serialized = TreeNodeFabric.Serialize(tree);
            Assert.IsFalse(string.IsNullOrWhiteSpace(serialized));

            var deserialized = TreeNodeFabric.Deserialize(serialized);
            var serialized2 = TreeNodeFabric.Serialize(deserialized);

            Assert.AreEqual(serialized2, serialized);
        }

        [TestMethod]
        public void BasicTest()
        {
            var tree = TreeNodeFabric.GenerateTreeNode("X3", "L7", "R9");           
            TreeCanBeProcessed(tree);
        }

        [TestMethod]
        public void BasicLeftTest()
        {
            var tree = TreeNodeFabric.GenerateTreeNode("X3", "L7", null);
            TreeCanBeProcessed(tree);
        }

        [TestMethod]
        public void BasicRightTest()
        {
            var tree = TreeNodeFabric.GenerateTreeNode("X3", null, "R9");
            TreeCanBeProcessed(tree);
        }


        [TestMethod]
        public void LeftTree()
        {
            var tree = new TreeNode("X");
            for (var i = 0; i < 100; i++)
            {
                tree = new TreeNode($"e-{i}", tree);
            }

            TreeCanBeProcessed(tree);
        }

        [TestMethod]
        public void RightTree()
        {
            var tree = new TreeNode("X");
            for (var i = 0; i < 100; i++)
            {
                tree = new TreeNode($"e-{i}", null, tree);
            }

            TreeCanBeProcessed(tree);
        }
        
        [TestMethod]
        public void BigLeftTree()
        {
            var tree = new TreeNode("X");
            for (var i = 0; i < 1000; i++)
            {
                tree = new TreeNode($"e-{i}",tree,null);
            }

            TreeCanBeProcessed(tree);
        }

        //todo : fails ???
        [TestMethod]
        public void BigTree()
        {
            var tree = new TreeNode("X");
            for (var i = 0; i < 1000; i++)
            {
                tree = new TreeNode($"e-{i}", JsonConvert.DeserializeObject<TreeNode>(JsonConvert.SerializeObject(tree)), tree);
            }

            TreeCanBeProcessed(tree);
        }
        [TestMethod]
        public void MidComplexTreeTest()
        {
            var oX3 = TreeNodeFabric.GenerateTreeNode("X3", "L7", "R9");
            var oX1 = new TreeNode("X1", oX3);

            TreeCanBeProcessed(oX1);
        }

        [TestMethod]
        public void ComplexTreeTest()
        {
            var oX3 = TreeNodeFabric.GenerateTreeNode("X3", "L7", "R9");
            var oX2 = TreeNodeFabric.GenerateTreeNode("X2", "L6", "R8");
            var oX1 = new TreeNode("X1", oX2, oX3);
            var oX4 = new TreeNode("X4", oX1);
            var oX5 = TreeNodeFabric.GenerateTreeNode("X5", "L1", "R1");
            var oRoot = new TreeNode("root", oX4, oX5);

            TreeCanBeProcessed(oRoot);
        }
    }
}
