﻿using System;
using BinaryTree;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class BasicTests
    {
        [TestMethod]
        public void NullTreeSerializationTest()
        {
            Assert.IsTrue(string.IsNullOrWhiteSpace(TreeNodeFabric.Serialize(null)));
        }

        [TestMethod]
        public void NullTreeDeserializationTest()
        {
            Assert.IsNull(TreeNodeFabric.Deserialize(null));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void IncorrectInputXmlDocument()
        {
            var t = TreeNodeFabric.Deserialize("19 null 45 null 1024");
        }


        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void IncorrectSerilizedTree()
        {
            var t = TreeNodeFabric.Deserialize("<root><X4><X1><X2><L6></L6><R8></R8></X2><X3><L7></L7><Err></Err><R9></R9></X3></X1></X4><X5><L1></L1><R1></R1></X5></root>");
        }
    }
}
