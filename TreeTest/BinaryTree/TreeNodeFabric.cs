﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace BinaryTree
{
    public static class TreeNodeFabric
    {
        public static TreeNode GenerateTreeNode(string name, string leftName, string rightName)
        {
            return new TreeNode(name, new TreeNode(leftName), new TreeNode(rightName));
        }


        private static string TagIt(string text, bool openTag = true)
        {
            return openTag ? "<" + text + ">" : "</" + text + ">";
        }

        public static string Serialize(TreeNode rootNode)
        {
            return rootNode == null || !rootNode.IsInitializedProperly() 
                ? null
                : TagIt(rootNode.Name)
                  + Serialize(rootNode.Child1)
                  + Serialize(rootNode.Child2)
                  + TagIt(rootNode.Name, false);
        }

        public static TreeNode Deserialize(string serializedTree)
        {
            if (string.IsNullOrEmpty(serializedTree)) return null;

            XElement element;

            try
            {
                element = XElement.Parse(serializedTree);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new ArgumentException($"Incorrect xml document '{serializedTree}'");
            }

            return element.IsEmpty ? null : Deserialize(element);
        }

        private static TreeNode Deserialize(XElement element)
        {
            if (element == null) return null;

            var nodesCount = element.Nodes().Count();

            if ( nodesCount > 2)
                throw new Exception($"Incorrect number of child nodes of '{element.Name.LocalName}'");
            
            return new TreeNode(
                element.Name.LocalName,
                nodesCount > 0 ? Deserialize((XElement) element.FirstNode) :  null,
                nodesCount > 1 ? Deserialize((XElement) element.LastNode) : null);
        }
    }
}
