﻿namespace BinaryTree
{
    public class TreeNode
    {
        public readonly string Name;
        public readonly TreeNode Child1;
        public readonly TreeNode Child2;

        public TreeNode(string name, TreeNode child1 = null, TreeNode child2 = null)
        {
            Name = name;
            Child1 = child1;
            Child2 = child2;
        }

        public bool IsInitializedProperly()
        {
            return !string.IsNullOrWhiteSpace(Name);
        }
    }
}